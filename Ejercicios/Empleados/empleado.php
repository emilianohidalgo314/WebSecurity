<?php

class Empleado {
    public $clave;
    private $salario;
    public $nombre;
    public $puesto;


    function __construct($clave, $nombre, $salario, $puesto){
        $this->clave = $clave;
        $this->nombre = $nombre;
        $this->salario = $salario;
        $this->puesto = $puesto;

    }

    function getClave(){
        return $this->clave;
    }
}

class Secretaria extends Empleado{
    function __construct($clave, $nombre, $salario, $puesto){
    parent::__construct($clave, $nombre, $salario, $puesto);
    }
}

class Gerente extends Empleado{
    function __construct($clave, $nombre, $salario, $puesto){
        parent::__construct($clave, $nombre, $salario, $puesto);
    }

    function solicitarPass($empleado){
        if($empleado instanceof Secretaria){
            return $empleado->getClave();
        }
    }
}

class Director extends Empleado{
    function __construct($clave, $nombre, $salario, $puesto){
        parent::__construct($clave, $nombre, $salario, $puesto);
    }

    function solicitarPass($empleado){
        return $empleado->getClave();
    }
}
$sec = new Secretaria(1,"Liz",20000,"Sec");
$dic = new Director(2, "Dic", 30000, "dic");
echo $dic->solicitarPass($sec);

?>