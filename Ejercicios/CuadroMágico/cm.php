<?php
function generarCuadro($n)
{
 
    $cMagico;
    for ($i = 0; $i < $n; $i++)
        for ($j = 0; $j < $n; $j++)
            $cMagico[$i][$j] = 0;
 
    // Posiciones iniciales
    $i = (int)$n / 2;
    $j = $n - 1;
 
    for ($num = 1; $num <= $n * $n; ) {
        
        if ($i == -1 && $j == $n) {
            $j = $n-2;
            $i = 0;
        }

        else {
            if ($j == $n)
                $j = 0;

            if ($i < 0)
                $i = $n-1;
        }
         
        if ($cMagico[$i][$j]) {
            $j -= 2;
            $i++;
            continue;
        }
        else
         
            $cMagico[$i][$j] = $num++; 
 
        $j++; $i--; 
    }    
    
    for ($i = 0; $i < $n; $i++) {
        for ($j = 0; $j < $n; $j++)
            echo $cMagico[$i][$j] . " ";
            echo "<br>";
    }
}

generarCuadro ($_POST['valor']);
     
?>